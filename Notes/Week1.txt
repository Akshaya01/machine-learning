In Week 1 we have started off by finding resources and learning blogs using which we could find the suitable algorithms and machine learning models to work with our project.
We came across many new libraries and resources like Keras, OpenCV, TensorFlow and models like CNN, UNet etc.
We learnt the basics of all these libraries and models by referring to the blogs and documentations.

Referred Links:
->https://nanonets.com/blog/semantic-image-segmentation-2020/
->https://towardsdatascience.com/how-to-train-your-self-driving-car-using-deep-learning-ce8ff76119cb
->https://www.analyticsvidhya.com/blog/2019/02/tutorial-semantic-segmentation-google-deeplab/
->https://www.analytixlabs.co.in/blog/what-is-image-segmentation/
->https://iq.opengenus.org/basics-of-machine-learning-image-classification-techniques/

Challenges faced:
->Using the datasets after mounting to Google Drive
        ->Resolved this issue by sharing the datasets to Google Drive and then adding it as shortcut to our Google   
        Drives individually.
->Understanding the new machine learning libraries and working on the initial stages of our project
        ->We then indivially took up the task to learn, explore and understand the basics of each library and then shared our
        learnings amongst our team mates, this way everyone of us began to have a basic idea of the models and resources that
        we planned to utilise to proceed with our project. 
