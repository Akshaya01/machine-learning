# Semantic Segmentation for Autonomous Driving
## Problem Statement :
Build a Deep learning system which can segment humans, animals, traffic signs, roads and lanes, pedestrian walks, trees, lamp posts, other vehicles, dividers, sky from camera feed of cars and label them accordingly throughout the journey.

## Data sets :
- Cambridge-driving Labeled Video Database

## Members:
- Akshaya TL - CSE - Roll No : 19WH1A0531
- Buvika K   - ECE - Roll No : 19WH1A0497
- Nidhi VN   - ECE - Roll No : 19WH1A0492
- Sudeepya Y - CSE - Roll No : 19WH1A0501
- Srilaxmi J - IT  - Roll No : 19WH1A1276
- Sneha P    - EEE - Roll No : 19WH1A0229

## Links for Colab Notebooks:
- Segmentation Model  
https://colab.research.google.com/drive/10E53sIwqk8XXDrmw88VJX8iAQB3vrv7K?authuser=1

- Object detection Method - 1
https://colab.research.google.com/drive/1bawtLzutNVN_dZWzTmxwGI1Pf1bnIe_L?authuser=1

- Object detection Method - 2
https://colab.research.google.com/drive/1te3J6bfknesJDRWJJd54razlwXmtP8QY?authuser=1

- Object detection Method - 3
https://colab.research.google.com/drive/1UoDdcfTaPYLV9NNNJKnq_Xmg8nUbrlC7?authuser=1

## Links for Google Drive Folders:
- Link to the main directory:
https://drive.google.com/drive/folders/1XzucC5dpaNBi9995k4XRIh9fImt-VDIs

- For predictions obtained from segmentation and detection:
https://drive.google.com/drive/folders/12ZSy-Ni0lAGBghGnlsiQfn4F-NQshZ61

- For Predicted Images obtained from the Model:
https://drive.google.com/drive/folders/1AxWHKhd22gcjTfd_cnRXMxpD_0rG1_gj

